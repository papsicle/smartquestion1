// @SOURCE:/usagers/sipapa/Log1000/TP2/conf/routes
// @HASH:092fc11548bc0e9a31d25dbc17d3df057ee6b6eb
// @DATE:Wed Jan 30 15:25:17 EST 2013

import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString

object Routes extends Router.Routes {


// @LINE:6
val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart("/"))))
                    

// @LINE:9
val controllers_Application_questions1 = Route("GET", PathPattern(List(StaticPart("/LOG1000"))))
                    

// @LINE:10
val controllers_Application_newQuestion2 = Route("POST", PathPattern(List(StaticPart("/LOG1000"))))
                    

// @LINE:11
val controllers_Application_deleteQuestion3 = Route("POST", PathPattern(List(StaticPart("/LOG1000/"),DynamicPart("id", """[^/]+"""),StaticPart("/delete"))))
                    

// @LINE:12
val controllers_Application_setQuestionAsAnswered4 = Route("POST", PathPattern(List(StaticPart("/LOG1000/"),DynamicPart("id", """[^/]+"""),StaticPart("/setasanswered"))))
                    

// @LINE:13
val controllers_Application_login5 = Route("GET", PathPattern(List(StaticPart("/login"))))
                    

// @LINE:14
val controllers_Application_loginPost6 = Route("POST", PathPattern(List(StaticPart("/login"))))
                    

// @LINE:15
val controllers_Application_cancelLogin7 = Route("POST", PathPattern(List(StaticPart("/cancel"))))
                    

// @LINE:16
val controllers_Application_logout8 = Route("GET", PathPattern(List(StaticPart("/logout"))))
                    

// @LINE:17
val controllers_Application_openIDCallback9 = Route("GET", PathPattern(List(StaticPart("/loginCallback"))))
                    

// @LINE:20
val controllers_Assets_at10 = Route("GET", PathPattern(List(StaticPart("/assets/"),DynamicPart("file", """.+"""))))
                    
def documentation = List(("""GET""","""/""","""controllers.Application.index"""),("""GET""","""/LOG1000""","""controllers.Application.questions"""),("""POST""","""/LOG1000""","""controllers.Application.newQuestion"""),("""POST""","""/LOG1000/$id<[^/]+>/delete""","""controllers.Application.deleteQuestion(id:String)"""),("""POST""","""/LOG1000/$id<[^/]+>/setasanswered""","""controllers.Application.setQuestionAsAnswered(id:String)"""),("""GET""","""/login""","""controllers.Application.login"""),("""POST""","""/login""","""controllers.Application.loginPost"""),("""POST""","""/cancel""","""controllers.Application.cancelLogin"""),("""GET""","""/logout""","""controllers.Application.logout"""),("""GET""","""/loginCallback""","""controllers.Application.openIDCallback"""),("""GET""","""/assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)"""))
             
    
def routes:PartialFunction[RequestHeader,Handler] = {        

// @LINE:6
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.index, HandlerDef(this, "controllers.Application", "index", Nil))
   }
}
                    

// @LINE:9
case controllers_Application_questions1(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.questions, HandlerDef(this, "controllers.Application", "questions", Nil))
   }
}
                    

// @LINE:10
case controllers_Application_newQuestion2(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.newQuestion, HandlerDef(this, "controllers.Application", "newQuestion", Nil))
   }
}
                    

// @LINE:11
case controllers_Application_deleteQuestion3(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(_root_.controllers.Application.deleteQuestion(id), HandlerDef(this, "controllers.Application", "deleteQuestion", Seq(classOf[String])))
   }
}
                    

// @LINE:12
case controllers_Application_setQuestionAsAnswered4(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(_root_.controllers.Application.setQuestionAsAnswered(id), HandlerDef(this, "controllers.Application", "setQuestionAsAnswered", Seq(classOf[String])))
   }
}
                    

// @LINE:13
case controllers_Application_login5(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.login, HandlerDef(this, "controllers.Application", "login", Nil))
   }
}
                    

// @LINE:14
case controllers_Application_loginPost6(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.loginPost, HandlerDef(this, "controllers.Application", "loginPost", Nil))
   }
}
                    

// @LINE:15
case controllers_Application_cancelLogin7(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.cancelLogin, HandlerDef(this, "controllers.Application", "cancelLogin", Nil))
   }
}
                    

// @LINE:16
case controllers_Application_logout8(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.logout, HandlerDef(this, "controllers.Application", "logout", Nil))
   }
}
                    

// @LINE:17
case controllers_Application_openIDCallback9(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.openIDCallback, HandlerDef(this, "controllers.Application", "openIDCallback", Nil))
   }
}
                    

// @LINE:20
case controllers_Assets_at10(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(_root_.controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String])))
   }
}
                    
}
    
}
                