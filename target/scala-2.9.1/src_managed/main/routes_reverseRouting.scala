// @SOURCE:/usagers/sipapa/Log1000/TP2/conf/routes
// @HASH:092fc11548bc0e9a31d25dbc17d3df057ee6b6eb
// @DATE:Wed Jan 30 15:25:17 EST 2013

import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString


// @LINE:20
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers {

// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
class ReverseApplication {
    


 
// @LINE:10
def newQuestion() = {
   Call("POST", "/LOG1000")
}
                                                        
 
// @LINE:13
def login() = {
   Call("GET", "/login")
}
                                                        
 
// @LINE:14
def loginPost() = {
   Call("POST", "/login")
}
                                                        
 
// @LINE:15
def cancelLogin() = {
   Call("POST", "/cancel")
}
                                                        
 
// @LINE:16
def logout() = {
   Call("GET", "/logout")
}
                                                        
 
// @LINE:17
def openIDCallback() = {
   Call("GET", "/loginCallback")
}
                                                        
 
// @LINE:12
def setQuestionAsAnswered(id:String) = {
   Call("POST", "/LOG1000/" + implicitly[PathBindable[String]].unbind("id", id) + "/setasanswered")
}
                                                        
 
// @LINE:11
def deleteQuestion(id:String) = {
   Call("POST", "/LOG1000/" + implicitly[PathBindable[String]].unbind("id", id) + "/delete")
}
                                                        
 
// @LINE:6
def index() = {
   Call("GET", "/")
}
                                                        
 
// @LINE:9
def questions() = {
   Call("GET", "/LOG1000")
}
                                                        

                      
    
}
                            

// @LINE:20
class ReverseAssets {
    


 
// @LINE:20
def at(file:String) = {
   Call("GET", "/assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                        

                      
    
}
                            
}
                    


// @LINE:20
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.javascript {

// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
class ReverseApplication {
    


 
// @LINE:10
def newQuestion = JavascriptReverseRoute(
   "controllers.Application.newQuestion",
   """
      function() {
      return _wA({method:"POST", url:"/LOG1000"})
      }
   """
)
                                                        
 
// @LINE:13
def login = JavascriptReverseRoute(
   "controllers.Application.login",
   """
      function() {
      return _wA({method:"GET", url:"/login"})
      }
   """
)
                                                        
 
// @LINE:14
def loginPost = JavascriptReverseRoute(
   "controllers.Application.loginPost",
   """
      function() {
      return _wA({method:"POST", url:"/login"})
      }
   """
)
                                                        
 
// @LINE:15
def cancelLogin = JavascriptReverseRoute(
   "controllers.Application.cancelLogin",
   """
      function() {
      return _wA({method:"POST", url:"/cancel"})
      }
   """
)
                                                        
 
// @LINE:16
def logout = JavascriptReverseRoute(
   "controllers.Application.logout",
   """
      function() {
      return _wA({method:"GET", url:"/logout"})
      }
   """
)
                                                        
 
// @LINE:17
def openIDCallback = JavascriptReverseRoute(
   "controllers.Application.openIDCallback",
   """
      function() {
      return _wA({method:"GET", url:"/loginCallback"})
      }
   """
)
                                                        
 
// @LINE:12
def setQuestionAsAnswered = JavascriptReverseRoute(
   "controllers.Application.setQuestionAsAnswered",
   """
      function(id) {
      return _wA({method:"POST", url:"/LOG1000/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", id) + "/setasanswered"})
      }
   """
)
                                                        
 
// @LINE:11
def deleteQuestion = JavascriptReverseRoute(
   "controllers.Application.deleteQuestion",
   """
      function(id) {
      return _wA({method:"POST", url:"/LOG1000/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", id) + "/delete"})
      }
   """
)
                                                        
 
// @LINE:6
def index = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"/"})
      }
   """
)
                                                        
 
// @LINE:9
def questions = JavascriptReverseRoute(
   "controllers.Application.questions",
   """
      function() {
      return _wA({method:"GET", url:"/LOG1000"})
      }
   """
)
                                                        

                      
    
}
                            

// @LINE:20
class ReverseAssets {
    


 
// @LINE:20
def at = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"/assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                                                        

                      
    
}
                            
}
                    


// @LINE:20
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.ref {

// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
class ReverseApplication {
    


 
// @LINE:10
def newQuestion() = new play.api.mvc.HandlerRef(
   controllers.Application.newQuestion(), HandlerDef(this, "controllers.Application", "newQuestion", Seq())
)
                              
 
// @LINE:13
def login() = new play.api.mvc.HandlerRef(
   controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Seq())
)
                              
 
// @LINE:14
def loginPost() = new play.api.mvc.HandlerRef(
   controllers.Application.loginPost(), HandlerDef(this, "controllers.Application", "loginPost", Seq())
)
                              
 
// @LINE:15
def cancelLogin() = new play.api.mvc.HandlerRef(
   controllers.Application.cancelLogin(), HandlerDef(this, "controllers.Application", "cancelLogin", Seq())
)
                              
 
// @LINE:16
def logout() = new play.api.mvc.HandlerRef(
   controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Seq())
)
                              
 
// @LINE:17
def openIDCallback() = new play.api.mvc.HandlerRef(
   controllers.Application.openIDCallback(), HandlerDef(this, "controllers.Application", "openIDCallback", Seq())
)
                              
 
// @LINE:12
def setQuestionAsAnswered(id:String) = new play.api.mvc.HandlerRef(
   controllers.Application.setQuestionAsAnswered(id), HandlerDef(this, "controllers.Application", "setQuestionAsAnswered", Seq(classOf[String]))
)
                              
 
// @LINE:11
def deleteQuestion(id:String) = new play.api.mvc.HandlerRef(
   controllers.Application.deleteQuestion(id), HandlerDef(this, "controllers.Application", "deleteQuestion", Seq(classOf[String]))
)
                              
 
// @LINE:6
def index() = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq())
)
                              
 
// @LINE:9
def questions() = new play.api.mvc.HandlerRef(
   controllers.Application.questions(), HandlerDef(this, "controllers.Application", "questions", Seq())
)
                              

                      
    
}
                            

// @LINE:20
class ReverseAssets {
    


 
// @LINE:20
def at(path:String, file:String) = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]))
)
                              

                      
    
}
                            
}
                    
                