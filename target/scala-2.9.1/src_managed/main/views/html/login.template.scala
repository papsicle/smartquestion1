
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object login extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[String],String,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(loginForm: Form[String], errorMessage: String):play.api.templates.Html = {
        _display_ {import helper._


Seq[Any](format.raw/*1.49*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Login")/*5.15*/ {_display_(Seq[Any](format.raw/*5.17*/("""
        <p>"""),_display_(Seq[Any](/*6.13*/errorMessage)),format.raw/*6.25*/("""</p>

        """),_display_(Seq[Any](/*8.10*/form(routes.Application.loginPost)/*8.44*/ {_display_(Seq[Any](format.raw/*8.46*/("""
                Entrer votre open ID
				<input type="type" name="openid">
                <input type="submit" value="Connexion">
        """)))})),format.raw/*12.10*/("""
        """),_display_(Seq[Any](/*13.10*/form(routes.Application.cancelLogin)/*13.46*/ {_display_(Seq[Any](format.raw/*13.48*/("""
			<input type="submit" value="Retour">
		""")))})),format.raw/*15.4*/("""
""")))})),format.raw/*16.2*/("""
"""))}
    }
    
    def render(loginForm:Form[String],errorMessage:String) = apply(loginForm,errorMessage)
    
    def f:((Form[String],String) => play.api.templates.Html) = (loginForm,errorMessage) => apply(loginForm,errorMessage)
    
    def ref = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jan 30 15:38:47 EST 2013
                    SOURCE: /usagers/sipapa/Log1000/TP2/app/views/login.scala.html
                    HASH: 5a98ff6ebd95245eae3e4435d2e8ad806e93099b
                    MATRIX: 518->1|658->48|686->67|722->69|743->82|782->84|830->97|863->109|913->124|955->158|994->160|1167->301|1213->311|1258->347|1298->349|1373->393|1406->395
                    LINES: 19->1|23->1|25->4|26->5|26->5|26->5|27->6|27->6|29->8|29->8|29->8|33->12|34->13|34->13|34->13|36->15|37->16
                    -- GENERATED --
                */
            