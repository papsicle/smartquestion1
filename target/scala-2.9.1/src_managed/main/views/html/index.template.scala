
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Course,Form[String],Option[String],play.api.templates.Html] {

    /**/
    def apply/*1.2*/(course: Course, questionForm: Form[String], email: Option[String]):play.api.templates.Html = {
        _display_ {import helper._


Seq[Any](format.raw/*1.69*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("course.title")/*5.22*/ {_display_(Seq[Any](format.raw/*5.24*/("""
  <header>
    """),_display_(Seq[Any](/*7.6*/email match/*7.17*/ {/*8.7*/case Some(email) =>/*8.26*/ {_display_(Seq[Any](format.raw/*8.28*/("""
        <span>"""),_display_(Seq[Any](/*9.16*/email)),format.raw/*9.21*/("""</span> <a href=""""),_display_(Seq[Any](/*9.39*/routes/*9.45*/.Application.logout)),format.raw/*9.64*/("""">Déconnexion</a>
      """)))}/*11.7*/case None        =>/*11.26*/ {_display_(Seq[Any](format.raw/*11.28*/("""
        <a href=""""),_display_(Seq[Any](/*12.19*/routes/*12.25*/.Application.login)),format.raw/*12.43*/("""">Connexion</a>
      """)))}})),format.raw/*14.6*/("""
  </header>

  <h1>"""),_display_(Seq[Any](/*17.8*/course/*17.14*/.title)),format.raw/*17.20*/("""</h1>
  
  <ul>
    """),_display_(Seq[Any](/*20.6*/course/*20.12*/.questions.map/*20.26*/ { question =>_display_(Seq[Any](format.raw/*20.40*/("""
      <li>
        """),_display_(Seq[Any](/*22.10*/question/*22.18*/.workstationId)),format.raw/*22.32*/(""" """),_display_(Seq[Any](/*22.34*/if(question.answered)/*22.55*/ {_display_(Seq[Any](format.raw/*22.57*/(""" (Résolue) """)))})),format.raw/*22.69*/("""
        
		"""),_display_(Seq[Any](/*24.4*/form(routes.Application.deleteQuestion(question.id))/*24.56*/ {_display_(Seq[Any](format.raw/*24.58*/("""
			<input type="submit" value="Supprimer">
		""")))})),format.raw/*26.4*/("""
		"""),_display_(Seq[Any](/*27.4*/form(routes.Application.setQuestionAsAnswered(question.id))/*27.63*/ {_display_(Seq[Any](format.raw/*27.65*/("""
			<input type="submit" value="Marquer comme résolue">
		""")))})),format.raw/*29.4*/("""
      </li>
    """)))})),format.raw/*31.6*/("""
  </ul>
  
  <h2>J'ai une question!</h2>
  
  """),_display_(Seq[Any](/*36.4*/form(routes.Application.newQuestion)/*36.40*/ {_display_(Seq[Any](format.raw/*36.42*/("""
	 Entrer la question ici!
     <input type="type" name="workstationId" size=80>
     <input type="submit" value="Soumettre">
     
   """)))})),format.raw/*41.5*/(""" 
""")))})),format.raw/*42.2*/("""
"""))}
    }
    
    def render(course:Course,questionForm:Form[String],email:Option[String]) = apply(course,questionForm,email)
    
    def f:((Course,Form[String],Option[String]) => play.api.templates.Html) = (course,questionForm,email) => apply(course,questionForm,email)
    
    def ref = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jan 30 16:00:02 EST 2013
                    SOURCE: /usagers/sipapa/Log1000/TP2/app/views/index.scala.html
                    HASH: 156849b1fe4959cdc0f8cd3a1228832401c53c2a
                    MATRIX: 533->1|693->68|721->87|757->89|785->109|824->111|875->128|894->139|903->148|930->167|969->169|1020->185|1046->190|1099->208|1113->214|1153->233|1196->265|1224->284|1264->286|1319->305|1334->311|1374->329|1429->358|1485->379|1500->385|1528->391|1584->412|1599->418|1622->432|1674->446|1731->467|1748->475|1784->489|1822->491|1852->512|1892->514|1936->526|1984->539|2045->591|2085->593|2163->640|2202->644|2270->703|2310->705|2400->764|2449->782|2532->830|2577->866|2617->868|2784->1004|2818->1007
                    LINES: 19->1|23->1|25->4|26->5|26->5|26->5|28->7|28->7|28->8|28->8|28->8|29->9|29->9|29->9|29->9|29->9|30->11|30->11|30->11|31->12|31->12|31->12|32->14|35->17|35->17|35->17|38->20|38->20|38->20|38->20|40->22|40->22|40->22|40->22|40->22|40->22|40->22|42->24|42->24|42->24|44->26|45->27|45->27|45->27|47->29|49->31|54->36|54->36|54->36|59->41|60->42
                    -- GENERATED --
                */
            